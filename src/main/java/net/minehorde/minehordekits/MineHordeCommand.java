
package net.minehorde.minehordekits;

import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;


public abstract class MineHordeCommand implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
		this.execute(cs, cmnd, string, strings);
		return true;
	}

	public abstract void execute(CommandSender sender, Command cmnd, String string, String[] args);

	
	public static void registerAll(Map<String, MineHordeCommand> commands, JavaPlugin plugin) {
		for (Entry<String, MineHordeCommand> command : commands.entrySet()) {
			plugin.getCommand(command.getKey()).setExecutor(command.getValue());
		}
	}
}
