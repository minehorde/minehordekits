package net.minehorde.minehordekits.listener;

import net.minehorde.minehorde.listeners.ArmourEquipEvent;
import net.minehorde.minehorde.listeners.ArmourEquipEvent.ArmourType;
import net.minehorde.minehorde.managers.PlayerManager;
import net.minehorde.minehorde.players.MHPlayer;
import net.minehorde.minehordekits.MineHordeKits;
import net.minehorde.minehordekits.kits.Kit;
import net.minehorde.minehordekits.kits.KitManager;
import net.minehorde.minehordekits.kits.passive.MHKPassive;
import net.minehorde.minehordekits.sql.MineHordeLogger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class MHKListener implements Listener {

    private MineHordeKits mhk;

    public MHKListener(MineHordeKits mhk) {
        this.mhk = mhk;
        registerEvents();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onKitSelect(PlayerInteractEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }
        Block block = event.getClickedBlock();
        if (!block.getType().equals(Material.SIGN) && !block.getType().equals(Material.WALL_SIGN)) {
            return;
        }
        Sign sign = (Sign) block.getState();
        if (!ChatColor.stripColor(sign.getLine(0)).equals("[MineHorde]") || !ChatColor.stripColor(sign.getLine(1)).equalsIgnoreCase("Kit")) {
            return;
        }
        String name = ChatColor.stripColor(sign.getLine(2));
        Player player = event.getPlayer();

        Kit kit = KitManager.get(name);
        if (kit == null) {
            player.sendMessage(ChatColor.RED + "This kit does not exist. If you think this is an error message an admin!");
            return;
        }
        MHPlayer ply = PlayerManager.getPlayer(player);
        if (ply.getKit() != null) {
            ply.sendMessage(ChatColor.RED + "You already have a Kit!");
            return;
        }
        if (kit.getValue() > KitManager.getRank(player.getName())) {
            player.sendMessage(ChatColor.RED + "You are not allowed to use this kit on this world. \n You can unlock it by purchasing access to the premium world at http://www.minehorde.net !");
            return;
        }
        player.getInventory().setContents(kit.getInventory());
        player.getInventory().setArmorContents(kit.getArmour());
        player.updateInventory();
        player.sendMessage(ChatColor.GOLD + "You have selected the " + kit.getName() + " Kit, Good luck!");
        String stringLoc;

        stringLoc = mhk.getConfig().getString("spawnpoint.loc", "world@-15@43@25");
        Location loc = KitManager.getLocFromString(stringLoc);
        ply.setCurrentKit(kit);
        ply.setArmour(kit.getBaseArmour());
        ply.setDamage(kit.getBaseDmg());
        ply.setElemRes(kit.getBaseElemRes());
        for (ItemStack stack : kit.getArmour()) {
            if (stack == null || stack.getType().equals(Material.AIR)) {
                continue;
            }
            Bukkit.getPluginManager().callEvent(new ArmourEquipEvent(player, ArmourEquipEvent.EquipMethod.DISPENSER, ArmourType.matchType(stack), new ItemStack(Material.AIR, 1), stack));
        }
        player.setSaturation(20.0F);
        player.setWalkSpeed((float) (kit.getMoveSpeed() / 1000.0));
        player.setFoodLevel(20);
        player.setExhaustion(0.0F);
        player.setHealthScaled(true);
        player.setHealthScale((double) kit.getBaseHealth());
        player.setMaxHealth((double) kit.getBaseHealth());
        player.setHealth(player.getMaxHealth());
        player.teleport(loc);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Location loc = player.getLocation();
        String stringLoc = player.getWorld().getName() + "@" + loc.getX() + "@" + loc.getBlockY() + "@" + loc.getBlockZ();
        KitManager.putLoc(player.getName(), stringLoc);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if (!player.hasPlayedBefore()) {
            MineHordeLogger.logRank(player.getName(), 0);
            KitManager.putRank(player.getName(), 0);
        }
        if (KitManager.getRank(player.getName()) == null) {
            Integer rank = MineHordeLogger.getRankValue(player.getName());
            if (rank == null) {
                rank = 0;
            }
            KitManager.putRank(player.getName(), rank);
        }
        //String stringLoc = KitManager.getLoc(player.getName());
        //if (stringLoc == null) {
        //   return;
        // }

        //  player.teleport(KitManager.getLocFromString(stringLoc));
        event.setJoinMessage(null);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onAbilityUse(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            return;
        }
        if (!event.hasItem()) {
            return;
        }
        MHPlayer player = PlayerManager.getPlayer(event.getPlayer());
        if (player.getKit() == null) {
            return;
        }
        ItemStack item = player.getPlayer().getInventory().getItem(8);
        if (item == null) {
            return;
        }
        if (!event.getItem().getType().equals(item.getType())) {
            return;
        }
        MHKPassive.processAbilityUse(player.getKit().getName().toLowerCase(), player);
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(this, mhk);
    }
}
