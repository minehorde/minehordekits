package net.minehorde.minehordekits;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minehorde.minehorde.managers.StorageManager;
import net.minehorde.minehordekits.kits.Kit;
import net.minehorde.minehordekits.kits.KitManager;
import net.minehorde.minehordekits.listener.MHKListener;
import net.minehorde.minehordekits.sql.MineHordeLogger;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionType;

public class MineHordeKits extends JavaPlugin {

    public static final Logger LOGGER = Logger.getLogger("Minecraft");
    public static final String PLUGIN_PREFIX = ChatColor.GREEN + "[" + ChatColor.DARK_GREEN + ChatColor.BOLD + "MineHordeKits" + ChatColor.GREEN + "]" + ChatColor.RESET;
    private YamlConfiguration dataFile;
    private MineHordeLogger mhLogger;
    private Map<String, MineHordeCommand> commands = new HashMap<>();

    @Override
    public void onEnable() {
        MHKListener listener = new MHKListener(this);
        mhLogger = new MineHordeLogger(this);
        loadKits();
        loadDataFile();
        loadLocations();
    }

    @Override
    public void onDisable() {
        saveConfig();
        saveLocations();
    }

    public MineHordeLogger getMHLogger() {
        return mhLogger;
    }

    public YamlConfiguration getDataFile() {
        return this.dataFile;
    }

    public static void log(String message) {
        log(Level.INFO, message);
    }

    public static void log(Level level, String message) {
        LOGGER.log(level, "[MineHorde] " + message);
    }

    public static void log(Level level, String message, Throwable thrown) {
        LOGGER.log(level, "[MineHorde] " + message, thrown);
    }

    public void saveDataFile() {
        File df = new File(this.getDataFolder() + "/data.yml");
        try {
            this.dataFile.save(df);
        } catch (IOException ex) {
            log(Level.SEVERE, "Could not save the data!", ex);
        }
    }

    public void saveLocations() {
        for (Entry<String, String> entry : KitManager.getLocations()) {
            getDataFile().set("player.location." + entry.getKey(), entry.getValue());
        }
    }

    public void loadLocations() {
        ConfigurationSection section = getDataFile().getConfigurationSection("player.location");
        if (section == null) {
            return;
        }
        for (String key : section.getKeys(false)) {
            KitManager.putLoc(key, getDataFile().getString("player.location." + key));
        }
    }

    public void loadKits() {
        ConfigurationSection section = getConfig().getConfigurationSection("kits");
        if (section == null) {
            return;
        }
        for (String key : section.getKeys(false)) {
            List<ItemStack> armourList = new ArrayList<>();
            for (String armour : getConfig().getStringList("kits." + key + ".armour")) {
                log(key + " " + armour);
                if (armour.equalsIgnoreCase("NONE")) {
                    armourList.add(null);
                    continue;
                }
                armourList.add(StorageManager.getItem(armour));
            }
            ConfigurationSection invSection = getConfig().getConfigurationSection("kits." + key + ".inventory");
            List<ItemStack> inventoryList = new ArrayList<>();
            int value = getConfig().getInt("kits." + key + ".value", 0);
            if (invSection != null) {
                for (String item : invSection.getKeys(false)) {
                    int amount = getConfig().getInt("kits." + key + ".inventory." + item + ".amount", 1);
                    ItemStack toAdd = StorageManager.getItem(item);
                    if (amount != 0) {
                        toAdd = new ItemStack(Material.valueOf(item), amount);
                    }
                    inventoryList.add(toAdd);
                }
            }
            log(armourList.toString());
            int baseArmour = getConfig().getInt("kits." + key + ".baseArm", 1);
            int baseHealth = getConfig().getInt("kits." + key + ".baseHealth", 1);
            int baseDmg = getConfig().getInt("kits." + key + ".baseDmg", 1);
            int baseElemRes = getConfig().getInt("kits." + key + ".baseElemRes", 1);
            int elemResPerLvl = getConfig().getInt("kits." + key + ".elemLvl", 1);
            int armourPerLvl = getConfig().getInt("kits." + key + ".armLvl", 1);
            int dmgPerLvl = getConfig().getInt("kits." + key + ".dmgLvl", 1);
            int moveSpeed = getConfig().getInt("kits." + key + ".moseSpeed", 250);

            Kit kit = new Kit(key, armourList, inventoryList, baseArmour, baseHealth, baseDmg, baseElemRes, armourPerLvl, elemResPerLvl, dmgPerLvl, moveSpeed, value);
            KitManager.put(key, kit);
        }
    }

    public static Enchantment getEnchantment(String name) {
        if (name.equalsIgnoreCase("Sharpness")) {
            return Enchantment.DAMAGE_ALL;
        } else if (name.equalsIgnoreCase("Looting")) {
            return Enchantment.LOOT_BONUS_MOBS;
        } else if (name.equalsIgnoreCase("Smite")) {
            return Enchantment.DAMAGE_UNDEAD;
        } else if (name.equalsIgnoreCase("FireAspect")) {
            return Enchantment.FIRE_ASPECT;
        } else if (name.equalsIgnoreCase("Unbreaking")) {
            return Enchantment.DURABILITY;
        } else if (name.equalsIgnoreCase("Arthropods")) {
            return Enchantment.DAMAGE_ARTHROPODS;
        } else if (name.equalsIgnoreCase("Respiration")) {
            return Enchantment.OXYGEN;
        } else if (name.equalsIgnoreCase("Protection")) {
            return Enchantment.PROTECTION_ENVIRONMENTAL;
        } else if (name.equalsIgnoreCase("PROTECTION_P")) {
            return Enchantment.PROTECTION_PROJECTILE;
        } else if (name.equalsIgnoreCase("A_KNOCKBACK")) {
            return Enchantment.ARROW_KNOCKBACK;
        } else {
            return null;
        }
    }

    public static Short getPotionShort(String name) {
        if (name.equalsIgnoreCase("NONE")) {
            return 0;
        }
        PotionType pt = PotionType.valueOf(name);
        if (pt == null) {
            return 0;
        }
        return (short) pt.getDamageValue();
    }

    public YamlConfiguration loadDataFile() {
        File df = new File(this.getDataFolder() + "/data.yml");

        if (!df.exists()) {
            try {
                df.createNewFile();
            } catch (IOException ex) {
                log(Level.SEVERE, "Could not create the data file!", ex);
            }
        }

        this.dataFile = YamlConfiguration.loadConfiguration(df);
        return this.dataFile;
    }
}
