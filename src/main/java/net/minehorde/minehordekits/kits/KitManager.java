
package net.minehorde.minehordekits.kits;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Location;


public class KitManager {

    private static Map<String, Kit> kits = new HashMap<String, Kit>();
    private static Map<String, String> playerLocations = new HashMap<String, String>();
    private static Map<String, Integer> playerRanks = new HashMap<String, Integer>();

    public static Collection<Kit> getKits() {
        return kits.values();
    }

    public static Kit remove(String key) {
        return kits.remove(key);
    }
    
    public static void putRank(String k, Integer v) {
        playerRanks.put(k, v);
    }
    
    public static Integer getRank(String k) {
        return playerRanks.get(k);
    }

    public static Set<Entry<String, String>> getLocations() {
        return playerLocations.entrySet();
    }

    public static String getLoc(String o) {
        return playerLocations.get(o);
    }

    public static String putLoc(String k, String v) {
        return playerLocations.put(k, v);
    }

    public static Kit put(String key, Kit value) {
        return kits.put(key, value);
    }

    public static Kit get(String key) {
        if (key == null || key.equalsIgnoreCase("null")) {
            return null;
        }
        return kits.get(key);
    }

    public static void clear() {
        kits.clear();
    }

    public static Location getLocFromString(String s) {
        List<String> location = Arrays.asList(s.split("@"));
        int x = Integer.parseInt(location.get(1));
        int y = Integer.parseInt(location.get(2));
        int z = Integer.parseInt(location.get(3));
        return new Location(Bukkit.getWorld(location.get(0)), x, y, z).add(.5, .5, .5);
    }
}
