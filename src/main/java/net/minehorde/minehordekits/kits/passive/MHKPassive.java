package net.minehorde.minehordekits.kits.passive;

import java.util.HashMap;
import java.util.Map;
import net.md_5.bungee.api.ChatColor;
import net.minehorde.minehorde.players.MHPlayer;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 *
 * @author dylan
 */
public class MHKPassive {

    private static final Map<String, Long> cooldown = new HashMap<>();

    public static boolean containsKey(String o) {
        return cooldown.containsKey(o);
    }

    public static Long putCooldown(String k, Long v) {
        return cooldown.put(k, v);
    }

    public static Long removeCooldown(String o) {
        return cooldown.remove(o);
    }

    public static void clearAllCooldowns() {
        cooldown.clear();
    }

    public static Long getCooldown(String o) {
        return cooldown.get(o);
    }

    public static void processAbilityUse(String kit, MHPlayer player) {
        switch (kit) {
            case "warrior":
                Long cooldown = getCooldown(player.getName());
                if (cooldown == null) {
                    cooldown = 0L;
                }
                Long elapsedTime = System.currentTimeMillis() - cooldown;
                if (elapsedTime <= 90) {
                    player.sendMessage(ChatColor.RED + "Kit Ability is currently on cooldown(" + (90 - elapsedTime) + ").");
                    return;
                }

                for (Entity entity : player.getPlayer().getNearbyEntities(7, 4, 7)) {
                    if (!(entity instanceof LivingEntity) || entity instanceof Player || !(entity instanceof Creature)) {
                        continue;
                    }
                    Creature creature = (Creature) entity;
                    creature.setTarget((LivingEntity) player.getPlayer());
                }
                break;
            case "mage":
                break;
            case "archer":
                break;
            default:
                return;
        }
    }
}
