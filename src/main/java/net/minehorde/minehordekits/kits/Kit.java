package net.minehorde.minehordekits.kits;

import java.util.List;
import org.bukkit.inventory.ItemStack;

public class Kit {

    private final ItemStack[] armour;
    private final ItemStack[] inventory;
    private final String name;
    private final int value;
    private final int armourPerLvl;
    private final int baseDmg;
    private final int baseHealth;
    private final int baseArmour;
    private final int baseElemRes;
    private final int dmgPerLvl;
    private final int eleResPerLvl;
    private final int moveSpeed;

    public Kit(String name, List<ItemStack> armour, List<ItemStack> inventory, int baseArmour, int baseHealth, int baseDmg, int baseElemRes, 
            int armourPerLvl, int elemResPerLvl, int dmgPerLvl, int moveSpeed, int value) {
        this.name = name;
        this.moveSpeed = moveSpeed;
        this.value = value;
        ItemStack[] tempArmour = new ItemStack[armour.size()];
        for (int x = 0; x < armour.size(); x++) {
            tempArmour[x] = armour.get(x);
        }
        this.armour = tempArmour;
        ItemStack[] tempInv = new ItemStack[inventory.size()];
        for (int i = 0; i < inventory.size(); i++) {
            tempInv[i] = inventory.get(i);
        }
        this.inventory = tempInv;
        this.armourPerLvl = armourPerLvl;
        this.dmgPerLvl = dmgPerLvl;
        this.baseHealth = baseHealth;
        this.eleResPerLvl = elemResPerLvl;
        this.baseArmour = baseArmour;
        this.baseElemRes = baseElemRes;
        this.baseDmg = baseDmg;
    }

    public ItemStack[] getArmour() {
        return armour;
    }

    public int getBaseArmour() {
        return baseArmour;
    }

    public int getBaseElemRes() {
        return baseElemRes;
    }

    public int getDmgPerLvl() {
        return dmgPerLvl;
    }

    public int getArmourPerLvl() {
        return armourPerLvl;
    }

    public int getEleResPerLvl() {
        return eleResPerLvl;
    }

    public int getBaseHealth() {
        return baseHealth;
    }

    public int getBaseDmg() {
        return baseDmg;
    }

    public int getValue() {
        return value;
    }

    public ItemStack[] getInventory() {
        return inventory;
    }

    public String getName() {
        return name;
    }

    public int getMoveSpeed() {
        return moveSpeed;
    }
}
