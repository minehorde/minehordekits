package net.minehorde.minehordekits.sql;

import java.util.logging.Level;
import net.minehorde.minehordekits.MineHordeKits;
import org.bukkit.configuration.Configuration;


public class MineHordeLogger {

    private MineHordeKits mh;
    private static Database db;

    public MineHordeLogger(MineHordeKits mh) {
        this.mh = mh;

        Configuration c = mh.getConfig();

        String host = c.getString("db.host");
        String dbname = c.getString("db.name");
        String user = c.getString("db.user");
        String pass = c.getString("db.pass");
        String prefix = c.getString("db.prefix");

        if (host == null) {
            host = "localhost";
            c.set("db.host", host);
        }

        if (dbname == null) {
            dbname = "mydb";
            c.set("db.name", dbname);
        }

        if (user == null) {
            user = "root";
            c.set("db.user", user);
        }

        if (pass == null) {
            pass = "admin";
            c.set("db.pass", pass);
        }

        if (prefix == null) {
            prefix = "horde_";
            c.set("db.prefix", prefix);
        }

        mh.saveConfig();

        db = new Database(host, dbname, user, pass, prefix);
        boolean connected = db.connect();
        if (connected) {
            genTables();
        } else {
            MineHordeKits.log(Level.SEVERE, "Could not connect to the database! Fill out your config.yml!");
        }
    }

    public Database getDb() {
        return db;
    }

    
    private void genTables() {
        db.execute("CREATE TABLE IF NOT EXISTS `" + db.getPrefix() + "ranks` ("
                + "`rank_id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
                + "`player` varchar(40) NOT NULL,"
                + "`rank_value` int(10),"
                + "PRIMARY KEY (`rank_id`));");
    }

    public static void logRank(String owner, Integer value) {
        db.execute("INSERT INTO " + db.getPrefix() + "ranks (player, rank_value) "
                + "VALUES("
                + "'" + owner + "',"
                + "'" + value + "'"
                + ")");
    }

    public static void updateRank(int amount, String owner) {
        db.execute("UPDATE " + db.getPrefix() + "ranks SET rank_value='" + amount + "'"
                + "WHERE player='" + owner + "'");
    }

    public static Integer getRankValue(String owner) {
        return db.getInteger("SELECT rank_value FROM " + db.getPrefix() + "ranks WHERE player='" + owner + "'");
    }
}
