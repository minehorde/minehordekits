package net.minehorde.minehordekits.sql;

import com.avaje.ebeaninternal.server.lib.sql.DataSourceException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import static net.minehorde.minehordekits.MineHordeKits.log;


public class Database {

    private String host;
    private String db;
    private String user;
    private String password;
    private String prefix;
    private Connection connection;

    public Database(String host, String db, String user, String password, String prefix) {
        this.host = host;
        this.db = db;
        this.user = user;
        this.password = password;
        this.prefix = prefix;
    }

    public String getDb() {
        return db;
    }

    public String getHost() {
        return host;
    }

    public String getPassword() {
        return password;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getUser() {
        return user;
    }

    
    public boolean connect() {
        String jdbc = "jdbc:mysql://" + host + "/" + db + "?user=" + user + "&password=" + password;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            throw new DataSourceException("Failed to initialize JDBC driver.");
        }
        try {
            connection = DriverManager.getConnection(jdbc);
            log("Connected to database!");
            return true;
        } catch (SQLException ex) { //Error handling below:
            log(Level.SEVERE, "Could not connnect to the database!", ex);
            return false;
        }
    }

    
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            log(Level.SEVERE, "An error occured while closing the connection.", ex);
        }
    }

    
    public boolean isConnected() {
        try {
            return connection.isValid(5);
        } catch (SQLException ex) {
            log(Level.SEVERE, "isConnected error!", ex);
        }
        return false;
    }

    
    public void execute(String sql) {
        try {
            if (isConnected()) {
                connection.prepareStatement(sql).executeUpdate();
            } else {
                connect();
                execute(sql);
            }
        } catch (SQLException ex) {
            log(Level.SEVERE, "Could not execute SQL statement!", ex);
        }
    }

    
    public ResultSet getResultSet(String sql) {
        try {
            if (isConnected()) {
                return connection.createStatement().executeQuery(sql);
            } else {
                connect();
                return getResultSet(sql);
            }
        } catch (SQLException ex) {
            log(Level.SEVERE, "Could not execute SQL statement!", ex);
            return null;
        }
    }

    
    public String getString(String sql) {
        try {
            ResultSet result = getResultSet(sql);
            result.next();
            return result.getString(1);
        } catch (SQLException ex) {
            log(Level.SEVERE, "Could not execute SQL statement!", ex);
        }
        return null;
    }

    
    public int getInteger(String sql) {
        try {
            ResultSet result = getResultSet(sql);
            result.next();
            return result.getInt(1);
        } catch (SQLException ex) {
            log(Level.SEVERE, "Could not execute SQL statement!", ex);
        }
        return 0;
    }

    
    public double getDouble(String sql) {
        try {
            ResultSet result = getResultSet(sql);
            result.next();
            return result.getDouble(1);
        } catch (SQLException ex) {
            log(Level.SEVERE, "Could not execute SQL statement!", ex);
        }
        return 0;
    }

    
    public boolean getBoolean(String sql) {
        try {
            ResultSet result = getResultSet(sql);
            result.next();
            boolean returnValue = result.getBoolean(1);
            return returnValue;
        } catch (SQLException ex) {
            log(Level.SEVERE, "Could not execute SQL statement!", ex);
        }
        return false;
    }

    
    public List<String> getColumn(String sql) {
        List<String> coldata = new ArrayList<String>();
        try {
            ResultSet result = getResultSet(sql);
            while (result.next()) {
                coldata.add(result.getString(1));
            }
            return coldata;
        } catch (SQLException ex) {
            log(Level.SEVERE, "Could not execute SQL statement!", ex);
        }
        return null;
    }
}